from client import *
from scenario import *

def main():
    menu()

def menu():

    print("Client du webservice de calculatrice.")
    operation = input("""
                          S: Scenario de calcul "Des Chiffres et des Lettres"
                          A: Addition
                          B: Soustraction
                          C: Multiplication
                          D: Division

                          Entrez votre choix (S; A,B,C ou D): """)

    num1 = input("Saisissez un premier entier.")
    num2 = input("Saisissez un second entier.")

    if operation == "S" or operation =="s":
        print(scenario())

    elif operation == "A" or operation =="a":
        print(addition(num1, num2))
    elif operation == "B" or operation == "b":
        print(soustraction(num1, num2))
    elif operation == "C" or operation == "c":
        print(multiplication(num1, num2))
    elif operation == "D" or operation == "d":
        print(division(num1, num2))
    else:
        print("Veuillez sélectionner A,B,C ou D.")
        menu()

main()