# Conception Logicielle - Calculatrice 

# Calculatrice - Client  

auteur : Guillaume Pichon 

L'objectif de ce projet Python est de concevoir un web service d'une calculatrice permettant de réaliser des opérations algébriques basiques.
Un client fait appel à ce webservice pour réaliser ces opérations. 
Enfin, un scénario de calcul d'une épreuve du jeu "Des Chiffres et des Lettres" est implémenté.

## Installation 

Pour installer les dépendances nécessaires au projet, saisir dans un terminal : 
pip install -r requirements.txt

## Quickstart 

Pour lancer le client, saisir dans un terminal : 
main.py 

## Webservice 

Ce projet ne fonctionne que si le webservice correspondant est fonctionnel.
Celui-ci est disponible sur le dépôt git : 
'https://gitlab.com/Nohcip/calculatrice-conception-logicielle.git'.

## Test Unitaire

Pour lancer les tests unitaires des fonctions addition() et division de client.py,
saisir dans un terminal : 
pytest test-client.py

