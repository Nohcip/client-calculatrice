import requests

url = "http://127.0.0.1:8000/"

def addition(a,b):
    numbers = {"num1": a, "num2": b}
    r = requests.post(url + "ajouter", json = numbers)
    return r.json()

def soustraction(a,b):
    numbers = {"num1": a, "num2": b}
    r = requests.post(url + "soustraire", json = numbers)
    return r.json()

def multiplication(a,b):
    numbers = {"num1": a, "num2": b}
    r = requests.post(url + "multiplier", json = numbers)
    return r.json()

def division(a,b):
    numbers = {"num1": a, "num2": b}
    r = requests.post(url + "diviser", json = numbers)
    return r.json()