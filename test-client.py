import pytest
from client import *

def test_addition():
    assert addition(0, 0)["resultat"] == 0
    assert addition(1, 2)["resultat"] == 3


def test_division():
    assert division(2, 1)["resultat"] == 2.00
    assert division(5, 3)["resultat"] == 1.67
    #cas de la division par zero
    assert type(division(5, 3)["resultat"]) == float
    assert type(division(5, 0)["resultat"]) != float