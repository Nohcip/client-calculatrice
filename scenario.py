from client import *

# On souhaite realiser l'operation :
# ((((69*11)/3)-167)*7)+299

def scenario():
    res = multiplication(69, 11)["resultat"]

    res = division(res, 3)["resultat"]

    res = soustraction(res, 167)["resultat"]

    res = multiplication(res, 7)["resultat"]

    res = addition(res, 299)

    print("Le compte est bon.")
    return res